import { Component } from '@angular/core';

@Component({
  moduleId:module.id,  
  selector: 'jumbotron',
  templateUrl: 'jumbotron.component.html'
})
export class JumbotronComponent {
  private jumbotronHeading:string;
  private jumbotronText:string;
  private jumbotronBtnText:string;
  private jumbotronUrl:string;

  constructor() {
    this.jumbotronHeading = "Hello Ya'll";
    this.jumbotronText = "This is my gangster page where we get it all done, son!"
    this.jumbotronBtnText = "Explore More";
    this.jumbotronUrl = "/about";
  }
 }
